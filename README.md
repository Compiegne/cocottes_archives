# Cocottes_Archives

## Création d'un template pour réaliser des cocottes avec un logiciel de présentation type Powerpoint ##

Suite au [partage sur Twitter](https://twitter.com/ArchivesOrleans/status/1239949195810922496) d'une cocotte réalisée par le service d'[Archives municipales d'Orléans](http://archives.orleans-metropole.fr/r/640/cocottes-/) [@ArchivesOrleans](https://twitter.com/ArchivesOrleans),
Nous avons créé un template ainsi que des modèles (réalisation en cours) de cocottes.

Que soit remercié donc le service qui m'a inspiré ainsi que [@MlleDentelle](https://www.mademoiselle-dentelle.fr/) pour le tutoriel 

Ceci est la première réalisation du Koalab' ([@LeuLeudo](https://twitter.com/LeuLeudo) / [@macgraveur](https://twitter.com/macgraveur))

Les modèles et exemples sont utilisables par tous sans restrictions.